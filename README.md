## TeaCode Recruitment challenge - Krzyszotf Meyer  - 03-03-2021

This project is a for recruitment for a TeaCode company

The design guidelines included:

- Getting a data from an API at AmazonAWS (getAllData, axios, and setState to React ContextAPI)
    (!) There is a kinda problem with getting this data from server. Looks like this server doesn't provides a CORS, and when you want to request the data with axios, the browsers are showing error "An Request are blocked by CORS policy etc."  
    The temporary solution is CORS-proxy-server, like cors-anywhere at heroku (included in this project, but this si now temporary and before you you this, you must go to https://cors-anywhere.herokuapp.com/corsdemo and click the "request temporary acces to the demo server", then the requests are getting OK.
    Another temporary solution is only for Google Chrome - Extension "Allow CORS" from the chrome web store.

- Listing the data, sorted alphabetically by the "last_name" parameter of data (json) structure.

- In any card of data (one listed element) have an avatar (some of them don' have it - then showing the default robot image), first name and last name. In addition, any card have a toggle button which can be clicked and logged to the console the id, (and all checked IDs).

- At the top of the screen are search input, which provides filtering elements by first name, and then last name. 
The filtering not changing the checked status. 

## Runnig this project locally
To run this project, you need to clone this repository, and open it in IDE (for. example Microsoft Visual Studio Code), and, when you are in the MS VSC console in the right folder,(project base folder, with package.json) then type 
npm install" to install dependencies first (theyre listed in package.json file) and wait for the install. When theyre installed, type "npm start" and wait for the new cart/ browser window with localhost:3000 (by default) with this project.

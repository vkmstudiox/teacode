import React, { useState } from 'react'
import {
    Card,
    CardTitle,
    CardImg,
    CardBody,
    FormCheckbox
  } from "shards-react";
import blankRobot from "../images/blank_robot.jpg"


const UserCard = ({ avatar, firstName, lastName, id, handleCheckerChange, checked }) => {

    const [checkedState, setCheckedState] = useState(false);

    return (
    <Card style={{ maxWidth: "200px" }} className="mt-3">
      <CardBody>
        <CardTitle>{firstName} {lastName}</CardTitle>
        <div className="d-inline-flex p-2 mr-2">
        <CardImg src={avatar ? avatar : blankRobot} width="50" className="ml-2 mr-2"/> 
        <FormCheckbox
          className="ml-3 mr-3 mt-2"
          toggle
          checked={checked.includes(id) ? true : false}
          onChange={(e) => {handleCheckerChange(id, e.checked); setCheckedState(!checkedState) }}>
        </FormCheckbox>
        </div>
      </CardBody>
    </Card>
    )
}

export default UserCard

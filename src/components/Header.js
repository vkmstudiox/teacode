import React, { useContext } from 'react'
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Navbar,
  NavbarBrand,
  Nav,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput,
} from "shards-react";
import { StoreContext } from '../Store/StoreProvider'

const Header = () => {
  const  { setSearch } = useContext(StoreContext);

    return (
    <Navbar type="dark" theme="primary" expand="md">
     <NavbarBrand href="#">Krzysztof Meyer</NavbarBrand>
     
     <Nav navbar className="ml-auto mr-auto col-xs-4 col-md-6">
            <InputGroup size="xl" seamless>
              <InputGroupAddon type="prepend">
                <InputGroupText>
                  <FontAwesomeIcon icon={faSearch} />
                </InputGroupText>
              </InputGroupAddon>
              <FormInput className="border-0" placeholder="Search: First-name Last-name..." onChange={(e) => {setSearch(e.target.value)}} />
            </InputGroup>
          </Nav>

          <NavbarBrand>Tea Code</NavbarBrand>
    </Navbar>
    )
}

export default Header
import React, { useContext, useEffect, useState } from 'react'
import { StoreContext } from '../Store/StoreProvider'
import { Container, Row, Col } from "shards-react";
import UserCard from './UserCard';
import { filterUsers } from '../functions/functions';

const Layout = () => {

  const  { users, search } = useContext(StoreContext);

  const [usersFiltered, setUsersFiltered] = useState([]);
  const [checkedIds, setCheckedIds] = useState([]);


   useEffect(() => {
    const delayed = setTimeout(() => {
        filterUsers(search, users, setUsersFiltered);
    }, 300);
    return () => clearTimeout(delayed);
      }, [search, users]);


const handleCheckerChange = (id) => {
    let idsArray = [];

    if(checkedIds.includes(id) === false) {
        idsArray.push(...checkedIds,id); 
        setCheckedIds(idsArray)
        }
    else 
    {idsArray = checkedIds;
        idsArray.splice(idsArray.indexOf(id),1);
        setCheckedIds(idsArray);
     }

    console.log(`nowCheckedId: ${id}`)
    console.log(`allCheckedIds: ${idsArray}`)
}


  const usersList = usersFiltered.map((user) => {
    return (
      <Col key={user.id} >
        <UserCard 
        key={user.email} 
        id={user.id}
        avatar={user.avatar} 
        firstName={user.first_name}
        lastName={user.last_name}
        checked={checkedIds}
        handleCheckerChange={handleCheckerChange}
        />
    </Col>
    );

   });


    return (
    <Container >
        <Row>
         {usersList}
        </Row>
    </Container>
    )
}

export default Layout

import React, { createContext, useState, useEffect } from 'react';
import { getAllData } from '../functions/functions'

export const StoreContext = createContext();

export const StoreProvider = ({ children }) => {
    
   const [users, setUsers] = useState([])
    const [search, setSearch] = useState('')
    useEffect(() => {
        getAllData().then((res) => {
            setUsers(res.data);
          });

      }, []);
      

    return (
        <StoreContext.Provider value={{
    users,  
    search,
    setSearch,
    }}>
        {children}
  </StoreContext.Provider>
    )
}

export default StoreProvider

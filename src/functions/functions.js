import axios from 'axios';


export const getAllData = async () =>
  await axios.get(`https://cors-anywhere.herokuapp.com/${process.env.REACT_APP_API_URL}`, {
    headers: 
    { 
        "Content-Type": "application/json; charset=utf-8",
        'Access-Control-Allow-Origin': '*'
    },
  });

export const compareValues = (key) => {
    return (a, b) => {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        return 0;
      }
  
      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }

      return (
        comparison
      );
    };
  }

      export const filterUsers = (search, users, setUsersFiltered) => {

        const usersSorted = users.sort(compareValues('last_name')); 
        const searchFirstname = search.split(" ")[0]
        const searchLastName = search.split(" ")[1]
        let usersFiltered;

        if (search && search.includes(" ")) {
            usersFiltered = usersSorted.filter((userSorted) => {
          return (
            userSorted.first_name.includes(searchFirstname) 
            && userSorted.last_name.includes(searchLastName) 
            )
        })}

        else if (search && search.includes(" ") === false) {
            usersFiltered = usersSorted.filter((userSorted) => {
          return (
            userSorted.first_name.includes(searchFirstname) 
            )
        })}

        else {usersFiltered = usersSorted}

    setUsersFiltered(usersFiltered)
    }

